# Projeto Symfony 4.x

### criar e rodar projeto

**1-** cria projeto com estrutura minima
```composer create-project symfony/skeleton "my-project"```

**2-** rodar server local
```
composer require server --dev
#run
bin/console server:run
```   

**3-** principais bundles para iniciar o desenvolvimento
```
composer require profiler --dev
composer require twig monolog security asset annot form validator
```
**4-** configurand orm com mysql
```
composer require orm
```

4.1- copiar conteudo de ```.env```, criar arquivo ```.env.local``` e colar o conteudo copiado, 

**Obs:** para variaveis 
em ambiente de dev setar valores em .env.local e para prod .env;

```DATABASE_URL=mysql://dbuser:dbpassword@127.0.0.1:3306/database_name```, alterar para seu usuario, senha e nome da database.

4.2- criar database

```bin/console doctrine:database:create  ```

4.3- inicializar tabelas apos criar ou alterar Entities
 
 ```bin/console doctrine:schema:update  --force```

**5-** trabalhando com assets front-end usando webpack atravez do bundle encore

```composer require encore --dev```

```npm install``` 
ou 
```yarn```

## estrutura de pastas e arquivos

1- ```src/Controller```, todo codigo com controllers e rotas da aplicação web.


Ex: ```src/Controller/DefaultController.php```, responsavel somente pela homepage, acessada
pela rota ```/``` com nome ```homepage``` .

2- ```templates```, diretorio onde devem ficar dodas as views.

Ex:  ```templates/base.html.twig```, onde sera importado os assets de css e javascript para toda a aplicação.

 ```templates/default/index.html.twig```, view do DefaultController.
 
3- ```config```, diretorio responsavel por conter configurações dos bunbles instalados e da aplicação.