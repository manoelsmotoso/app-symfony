<?php
declare(strict_types = 1);


namespace App\Controller;

use App\Entity\UserEntity;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{

    /**
     * @Route("/")
     */
    public function indexAction(EntityManagerInterface $em)
    {
        $entities = $em->getRepository(UserEntity::class)->findAll();

        return $this->render('user/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * @Route("/new")
     */
    public function newAction(Request $request, EntityManagerInterface $em)
    {
        $entity = new UserEntity();

        $form = $this->createForm(UserType::class, $entity);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em->persist($entity);
            $em->flush();

            return $this->redirectToRoute('app_user_index');
        }

        return $this->render('user/new.html.twig', [
            'form' => $form->createView()
        ]);
    }
}